'use strict';
// Created by MeRush. v1.1

// 1. Plugins for development
var gulp = require('gulp'),                          // зборщик проекта
    watch = require('gulp-watch'),                   // отслеживание изменений в файлах
    sourcemaps = require('gulp-sourcemaps'),         // вывод пути к файлу в "DevTools" браузера
    rigger = require('gulp-rigger'),                 // импорт файлов в другие файлы (шаблонизатор)
    rename = require('gulp-rename'),                 // изменение имени файлов на выходе
    browserSync = require('browser-sync'),           // локальный сервер с функцией "live reload"
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),               // выводит ошибки в консоль во время зборки
    notify = require('gulp-notify'),                 // расширение для "plumber"
    newer = require('gulp-newer'),                   //
    sass = require('gulp-sass'),                     // препроцессор для стилей
    prefixer = require('gulp-autoprefixer'),         // добавление в стили вендорных префиксов
    gcmq = require('gulp-group-css-media-queries'),  // собирает одинаковые "@media" в один блок
    minifycss = require('gulp-minify-css'),          // минимизирует css
    uglify = require('gulp-uglify'),                 // минификатор для js
    imagemin = require('gulp-imagemin'),             // зжатие картинок
    rimraf = require('gulp-rimraf');                 // удаление указанной дериктории или файла

// 2. Configure paths
var paths = {
    input: { // input
        html: './tpl/**/*.html',
        htmlStart: './tpl/*.html',
        style: './style/**/*.scss',
        js: './js/**/*.js',
        fonts: './fonts/**/*',
        img: './img/*.*',
        images: './images/*.*',
        vendors: './vendors/**/*'
    },
    build: { // output
        html: '../html/assets/tpl/',
        htmlStart: '../html/assets/',
        style: '../html/assets/style/',
        js: '../html/assets/js/',
        fonts: '../html/assets/fonts/',
        img: '../html/assets/',
        images: '../html/assets/',
        vendors: '../html/assets/vendors/'
    },
    clean: '../html/assets/' // delete file or directory on this path
};

gulp.task('html:build', function() { // html
    return gulp.src(paths.input.html)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'HTML',
                    message: err.message
                }
            })
        }))
        .pipe(rigger())
        .pipe(gulp.dest(paths.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('htmlStart:build', function() { // html
    return gulp.src(paths.input.htmlStart)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'HTML',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.build.htmlStart))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function() { // style
    return gulp.src(paths.input.style)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'STYLE',
                    message: err.message
                }
            })
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(gcmq())
        .pipe(minifycss())
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.build.style))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function() { // js
    return gulp.src(paths.input.js)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'JS',
                    message: err.message
                }
            })
        }))
        .pipe(sourcemaps.init())
        .pipe(rigger())
        .pipe(sourcemaps.write())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() { // fonts
    return gulp.src(paths.input.fonts)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'FONTS',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.build.fonts));
});

gulp.task('img:build', function() { // img
    return gulp.src(paths.input.img, {base: './'})
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'IMAGES',
                    message: err.message
                }
            })
        }))
        .pipe(newer(paths.build.img))
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('images:build', function() { // images
    return gulp.src(paths.input.images, {base: './'})
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'IMAGES',
                    message: err.message
                }
            })
        }))
        .pipe(newer(paths.build.images))
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.images))
        .pipe(reload({stream: true}));
});

gulp.task('vendors:build', function() { // vendors
    return gulp.src(paths.input.vendors)
        .pipe(plumber({
            errorHandler : notify.onError(function (err) {
                return {
                    title: 'VENDORS',
                    message: err.message
                }
            })
        }))
        .pipe(newer(paths.build.images))
        .pipe(gulp.dest(paths.build.vendors))
        .pipe(reload({stream: true}));
});

gulp.task('webserver', function() { // create server
    browserSync.init({
        server: {baseDir: "../html/assets/"}
    });
});

gulp.task('build', [
    'html:build',
    'htmlStart:build',
    'style:build',
    'js:build',
    'fonts:build',
    'img:build',
    'images:build',
    'vendors:build'
]);

// delete in "html/assets" derictory some file or derictory
gulp.task('delete', function(target) {
    rimraf(paths.clean, target);
});

gulp.task('watch', function() { // watch
    watch([paths.input.html], function() {
        gulp.start('html:build');
    });
    watch([paths.input.htmlStart], function() {
        gulp.start('htmlStart:build');
    });
    watch([paths.input.style], function() {
        gulp.start('style:build');
    });
    watch([paths.input.js], function() {
        gulp.start('js:build');
    });
    watch([paths.input.fonts], function() {
        gulp.start('fonts:build');
    });
    watch([paths.input.img], function() {
        gulp.start('img:build');
    });
    watch([paths.input.images], function() {
        gulp.start('images:build');
    });
    watch([paths.input.vendors], function() {
        gulp.start('vendors:build');
    });
});

// Run the gulp configuration in this one as default
gulp.task('default', ['build', 'webserver', 'watch']);

