# Description of the assembly of Gulp, BEM and the work as a whole

## Content
- [Install Node.js and create modules for Gulp](#markdown-header-install-nodejs-and-create-modules-for-gulp)
- [Assembly documentation](#markdown-header-assembly-documentation)
    - [File system](#markdown-header-file-system)
    - [Contents of folders](#markdown-header-contents-of-folders)
    - [Description of folders and files](#markdown-header-description-of-folders-and-files)

___

## Install Node.js and create modules for Gulp

- ##### Если не установлен на компьютере node.js, то для начала работы нужно все подготовить и установить:
***Для Windows***
    - На сайте <https://nodejs.org> скачиваем и устанавливаем node.js
    - Далее, через **командную строку node.js** мы устанавливаем глобально клиент gulp: `npm i gulp-cli -g`

- ##### Установка модулей для Gulp
    - В терминале (windows node.js терминал, lynux терминал или терминал IDE) переходим в папку "markup" роекта и прописываем следующее:
`npm i --save-dev gulp gulp-watch gulp-sourcemaps gulp-rigger gulp-rename browser-sync gulp-plumber gulp-notify gulp-newer gulp-sass gulp-autoprefixer gulp-group-css-media-queries gulp-minify-css gulp-uglify gulp-imagemin gulp-rimraf`

___

## Assembly documentation

#### File system
```
project/
│
├─── markup/
│   │
│   ├─── fonts/
│   ├─── images/
│   ├─── img/
│   │
│   ├─── js/
│   │   │
│   │   ├─── components/
│   │   └─── common.js
│   │
│   ├─── style/
│   │   │
│   │   ├─── base/
│   │   │   │
│   │   │   ├─── _common.scss
│   │   │   ├─── _fonts.scss
│   │   │   ├─── _index.scss
│   │   │   └─── _reset.scss
│   │   │
│   │   ├─── blocks/
│   │   │   │
│   │   │   └─── _index.scss
│   │   │
│   │   ├─── theme/
│   │   │   │
│   │   │   └─── _index.scss
│   │   │
│   │   ├─── utils/
│   │   │   │
│   │   │   ├─── _index.scss
│   │   │   ├─── _mixins.scss
│   │   │   └─── _vars.scss
│   │   │
│   │   ├─── style.scss
│   │   └─── style_theme.scss
│   │
│   ├─── tpl/
│   │   │
│   │   ├─── components/
│   │   │
│   │   ├─── layout/
│   │   │   │
│   │   │   ├─── head-tpl.html
│   │   │   └─── footer-tpl.html
│   │   │
│   │   ├─── pages/
│   │   │   │
│   │   │   └─── page.html
│   │   │
│   │   └─── index.html
│   │
│   ├─── vendors/
│   │   │
│   │   ├─── libs/
│   │   └─── plugins/
│   │
│   ├─── Gulpfile.js
│   ├─── package.json
└───┘
```

[Back to top](#markdown-header-description-of-the-assembly-of-gulp-bem-and-the-work-as-a-whole)

___

#### Contents of folders

```
markup/			        # рабочие (исходные) файлы front-end разработчика
    | - tpl/			# *.html, *.jade, *.pug …
    | - style/			# *.css, *.scss, *.sass, *.less, *.styl …
    | - js/			    # *.js, *.ts, *.coffee …
    | - fonts/			# *.ttf, *.woff, *.woff2, *.eot …
    | - img/			# *.svg, *.png, *.jpg ... ( ! статические изображения, иконки )
    | - images/			# *.jpg, *.png, *.svg … ( ! заменяемые)
    | - vendors/	    # *.* (js, css, img, font, *.json … )
    |  package.json		# список зависимостей нужных для разработки
    |  Gulpfile.js	    # настройки для сборщика gulp (grunt … )
```

[Back to top](#markdown-header-description-of-the-assembly-of-gulp-bem-and-the-work-as-a-whole)

___

#### Description of folders and files

Шаблоны                           | 
----------------------------------|----------------------
./tpl/                            | В папке tpl или templates хранятся исходные файлы которые служат для создания итоговой разметки веб-страницы. Это могут быть файлы с расширениям *.html, *.jade, *.pug или другие.
./tpl/index.html                  | Файл index.html содержит только ссылки на уже разработанные страницы.
./tpl/layout/                     | В папку layout скалываем файлы, которые принимают участие в построении общей структуры сайта. Содержимое этих файлов является служебным и используется почти на всех страницах. Чтобы понимать, что файлы являются служебными и их можно использовать как шаблоны (template), к именам добавляется постфикс “-tpl”. Например: head-tpl.html, header-tpl.html, footer-tpl.html и т.д. 
./tpl/components/                 | Файлы, которые размещаются в папке components, также  являются служебными и применяются для построения веб страниц. Их главное различие с файлами папки layout  в том, что содержимое может быть добавлено только на определенные страницы. Например: breadcrumbs-tpl.html, modal-tpl.html и т.д.
./tpl/pages/                      | Папка pages предназначена для хранения разработанных (рабочих) страниц веб-сайта. Под рабочими страницами подразумевается то, что они собираются с шаблонов с использованием дополнительных элементов.

Стили                             |
----------------------------------|------------------------
./style/	                  | Здесь храним файлы со стилями,  *.css, *.scss, *.sass, *.less, *.styl ...
./style/utils/                    | Папка utils собирает все инструменты и помощники по стилям в проекте. Каждая глобальная переменная, функция и примесь должна быть помещена сюда.
./style/base/                     | Папка base/ содержит то, что мы можем назвать общим шаблоном проекта. В ней вы можете найти файл сброса, общие типографские правила, а так же файл стилей (_common.scss), определяющие некоторые стандартные стили для часто используемых элементов HTML.
./style/blocks/                   | Здесь хранятся все основные (*.css, *.scss, *.sass, *.less, *.styl ...) файлы, отвечающие за постройку всего сайта. Каждый файл обязательно должен быть назван по названию блока в HTML. В этом файле описывается поведение блока, его элементов и модификаторов, как для самого блока, так и для элементов блока. <https://ru.bem.info/methodology/quick-start/>
./style/theme/                    | Папку theme складываем стили для разных версий сайта (цветовая схема, версия для печати, админка)

JavaScript                        | JavaScript  (Как организовать - <https://habrahabr.ru/post/218485/> )
----------------------------------|-----------------------
./js/                             | *.js, *.ts, *.coffee …                             
./js/components/                  | Модули компонентов (то что желательно писать своими руками).                                                                               

Шрифты                            |
----------------------------------|-----------------------
./fonts/                          | *.ttf, *.woff, *.woff2, *.eot …
./fonts/roboto/                   | Папка для конкретного шрифта и его вариаций
./fonts/roboto/light/             | Папка для конкретной вариации шрифта

Изображения                       |
----------------------------------|-----------------------
./img/                            | *.svg, *.png, *.jpg ... статические изображения для front-end разработчика
./images/                         | *.jpg, *.png, *.svg … изображения, которые могут добавляться через админку, заменяться разработчиком, тестировщиком. В общем динамические.

Сторонние Библиотеки/плагины      |
----------------------------------|------------------------
./vendors/                        | *.* (js, css, img, font, *.json … )
./vendors/libs/                   | Сюда складываем библиотеки. Например jQuery создав для этого одноименную папку внутри папки libs/
./vendors/plugin/                 | Сюда плагины со своим содержимым

Файлы для сборки и git            |
----------------------------------|-----------------------
./package.json                    | Список зависимостей нужных для разработки
./Gulpfile.js                     | Настройки для сборщика gulp (grunt … ) <http://webdesign-master.ru/blog/tools/2016-03-09-gulp-beginners.html>
./.gitignore                      | Общий для всех разработчиков


[Back to top](#markdown-header-description-of-the-assembly-of-gulp-bem-and-the-work-as-a-whole)

___